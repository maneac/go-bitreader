package bitreader

import "fmt"

// ReadInt8 will read the next n bits as a int8.
func (r *Reader) ReadInt8(n uint) (int8, error) {
	uintBytes, err := r.ReadBits(n)
	if err != nil {
		return 0, fmt.Errorf("reading int8 bits: %w", err)
	}

	mask := uint8(1 << (n - 1))

	return int8((uintBytes[0] ^ mask) - mask), nil
}

// ReadInt32BE will read the next n bits as a Big-Endian int32.
func (r *Reader) ReadInt32BE(n uint) (int32, error) {
	uintBytes, err := r.ReadBits(n)
	if err != nil {
		return 0, fmt.Errorf("reading int32 bits: %w", err)
	}

	var out uint32
	for i := 0; i < len(uintBytes); i++ {
		out |= (uint32(uintBytes[i]) << (8 * (len(uintBytes) - i - 1)))
	}

	mask := uint32(1 << (n - 1))

	return int32((out ^ mask) - mask), nil
}

// ReadInt32LE will read the next n bits as a Little-Endian int32.
func (r *Reader) ReadInt32LE(n uint) (int32, error) {
	uintBytes, err := r.ReadBits(n)
	if err != nil {
		return 0, fmt.Errorf("reading int32 bits: %w", err)
	}

	var out uint32
	for i := 0; i < len(uintBytes); i++ {
		out |= (uint32(uintBytes[i]) << (8 * i))
	}

	mask := uint32(1 << (n - 1))

	return int32((out ^ mask) - mask), nil
}
