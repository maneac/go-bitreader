// Package bitreader provides a way to read an arbitrary number of
// bits from a byte slice.
package bitreader

import (
	"bufio"
	"fmt"
	"io"
)

// Reader implements the io.Reader interface.
type Reader struct {
	r         *bufio.Reader
	remaining uint
	buf       byte
}

func NewReader(source io.Reader) *Reader {
	return &Reader{
		r: bufio.NewReader(source),
	}
}

// Read populates the supplied slice with the next 8*len(dst) bits.
// Must be aligned to a byte boundary.
func (r Reader) Read(dst []byte) (int, error) {
	if r.remaining != 0 {
		return 0, fmt.Errorf("unable to read bytes into slice: %w", ErrMisaligned)
	}

	if dst == nil {
		return 0, ErrNilDestination
	}

	if len(dst) == 0 {
		return 0, nil
	}

	n, err := r.r.Read(dst)
	if err != nil {
		return 0, fmt.Errorf("reading bytes from underlying buffer: %w", err)
	}

	return n, nil
}

// ReadBytes will read the next n bytes into a new byte slice.
// Must be aligned to a byte boundary.
func (r *Reader) ReadBytes(n uint) ([]byte, error) {
	if r.remaining != 0 {
		return nil, fmt.Errorf("unable to read bytes: %w", ErrMisaligned)
	}

	out := make([]byte, n)
	_, err := r.r.Read(out)
	if err != nil {
		return nil, fmt.Errorf("reading bytes from underlying buffer: %w", err)
	}

	return out, nil
}

// ReadBits will read the next n bits into a new byte slice.
func (r *Reader) ReadBits(n uint) ([]byte, error) {
	// handle trivial case
	if r.remaining == 0 && n%8 == 0 {
		return r.ReadBytes(n / 8)
	}

	// result is entirely within buffer
	if n <= r.remaining {
		maskedBuffer := (r.buf & ((1 << r.remaining) - 1)) >> (r.remaining - n)

		r.remaining -= n

		return []byte{maskedBuffer}, nil
	}

	numBytesToRead := (n - r.remaining + 7) / 8

	readBytes := make([]byte, numBytesToRead)
	_, err := r.r.Read(readBytes)
	if err != nil {
		return nil, fmt.Errorf("reading bits from underlying buffer: %w", err)
	}

	newRemaining := (r.remaining - n) % 8

	output := make([]byte, (n+7)/8)

	for i := range output {
		readIdx := len(readBytes) - i - 1
		if readIdx < 0 {
			output[len(output)-i-1] = ((r.buf & ((1 << r.remaining) - 1)) >> newRemaining)
			continue
		}
		if readIdx == 0 {
			output[len(output)-i-1] = ((r.buf & ((1 << r.remaining) - 1)) << (8 - newRemaining)) | (readBytes[readIdx] >> newRemaining)
			continue
		}
		output[len(output)-i-1] = ((readBytes[readIdx-1] & ((1 << newRemaining) - 1)) << (8 - newRemaining)) | (readBytes[readIdx] >> newRemaining)
	}

	r.remaining = newRemaining
	r.buf = readBytes[len(readBytes)-1]

	return output, nil
}

// Align synchronises the reader to the next byte boundary.
func (r *Reader) Align() {
	r.remaining = 0
	r.buf = 0x00
}
