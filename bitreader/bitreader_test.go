package bitreader

import (
	"bytes"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestRead(t *testing.T) {
	tests := map[string]struct {
		buffer      []byte
		dest        []byte
		errContains string
		expected    []byte
	}{
		"nilDestination": {
			dest:        nil,
			errContains: ErrNilDestination.Error(),
		},
		"nilInput": {
			buffer:      nil,
			dest:        make([]byte, 1),
			errContains: io.EOF.Error(),
		},
		"destLargerThanInput": {
			buffer:      make([]byte, 0),
			dest:        make([]byte, 1),
			errContains: io.EOF.Error(),
		},
		"emptyDestination": {
			buffer:   nil,
			dest:     make([]byte, 0),
			expected: make([]byte, 0),
		},
		"pass": {
			buffer:   []byte{0x00, 0x01},
			dest:     make([]byte, 2),
			expected: []byte{0x00, 0x01},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			r := NewReader(bytes.NewReader(test.buffer))

			n, err := r.Read(test.dest)
			if test.errContains != "" {
				require.Error(t, err)
				assert.Contains(t, err.Error(), test.errContains)
				return
			}
			require.NoError(t, err)

			assert.Len(t, test.expected, n)
			assert.Equal(t, test.expected, test.dest)
		})
	}
}

func TestReadBits(t *testing.T) {
	tests := map[string]struct {
		buffer      []byte
		n           uint
		errContains string
		expected    []byte
	}{
		"nGreaterThanBuffer": {
			buffer:      make([]byte, 0),
			n:           1,
			errContains: io.EOF.Error(),
		},
		"multipleOf8": {
			buffer:   []byte{0b1001_0110},
			n:        8,
			expected: []byte{0b1001_0110},
		},
		"singleByte": {
			buffer:   []byte{0b1000_0000},
			n:        1,
			expected: []byte{0b0000_0001},
		},
		"multipleBytes": {
			buffer:   []byte{0b1111_1111, 0b0101_0101},
			n:        11,
			expected: []byte{0b0000_0111, 0b1111_1010},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			r := NewReader(bytes.NewReader(test.buffer))

			output, err := r.ReadBits(test.n)
			if test.errContains != "" {
				require.Error(t, err)
				assert.Contains(t, err.Error(), test.errContains)
				return
			}
			require.NoError(t, err)

			assert.Equal(t, test.expected, output)
		})
	}
}

func TestReadBits_Consecutive(t *testing.T) {
	tests := map[string]struct {
		buffer      []byte
		ns          []uint
		errContains string
		expecteds   [][]byte
	}{
		"multiplesOf8": {
			buffer:    []byte{0b1001_0110, 0b0000_0001, 0b1000_0000},
			ns:        []uint{8, 16},
			expecteds: [][]byte{{0b1001_0110}, {0b0000_0001, 0b1000_0000}},
		},
		"singleByte": {
			buffer:    []byte{0b1000_0101},
			ns:        []uint{3, 5},
			expecteds: [][]byte{{0b0000_0100}, {0b0000_0101}},
		},
		"multipleBytes": {
			buffer:    []byte{0b1110_1111, 0b0101_0101, 0b1111_1000},
			ns:        []uint{7, 10},
			expecteds: [][]byte{{0b0111_0111}, {0b0000_0010, 0b1010_1011}},
		},
		"containedMiddleRead": {
			buffer:    []byte{0b0001_0111, 0b0111_0000, 0b0000_0011, 0b0111_0000},
			ns:        []uint{20, 3, 5},
			expecteds: [][]byte{{0b0000_0001, 0b0111_0111, 0b0000_0000}, {0b0000_0001}, {0b0001_0111}},
		},
		"threeReads": {
			buffer:    []byte{0b1110_1111, 0b0101_0101, 0b1111_1000},
			ns:        []uint{4, 9, 11},
			expecteds: [][]byte{{0b0000_1110}, {0b0000_0001, 0b1110_1010}, {0b0000_0101, 0b1111_1000}},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			r := NewReader(bytes.NewReader(test.buffer))

			for i := range test.ns {
				output, err := r.ReadBits(test.ns[i])
				if test.errContains != "" {
					require.Error(t, err)
					assert.Contains(t, err.Error(), test.errContains)
					return
				}
				require.NoError(t, err)

				assert.Equal(t, test.expecteds[i], output, "Wanted: %08b\nGot: %08b", test.expecteds[i], output)
			}
		})
	}
}
