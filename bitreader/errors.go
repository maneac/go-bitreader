package bitreader

import "errors"

var (
	// ErrNilDestination is returned when a nil slice is supplied to a Read
	ErrNilDestination = errors.New("nil destination supplied")
	// ErrUnimplemented is returned if the developer has been lazy and skipped the
	// implementation for a given function
	ErrUnimplemented = errors.New("unimplemented")
	// ErrMisaligned is returned if a function requires the underlying reader to be
	// aligned to the nearest byte boundary, and it is not
	ErrMisaligned = errors.New("misaligned reader")
)
