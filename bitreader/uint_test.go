package bitreader

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestReadUint32BE(t *testing.T) {
	tests := map[string]struct {
		buffer      []byte
		n           uint
		errContains string
		expected    uint32
	}{
		"pass": {
			buffer:   []byte{7},
			n:        8,
			expected: 7,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			r := NewReader(bytes.NewReader(test.buffer))

			output, err := r.ReadUint32BE(test.n)
			if test.errContains != "" {
				require.Error(t, err)
				assert.Contains(t, err.Error(), test.errContains)
				return
			}
			require.NoError(t, err)

			assert.Equal(t, test.expected, output)
		})
	}
}
