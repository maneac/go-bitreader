package bitreader

import "fmt"

// Peek returns the next n bytes without advancing the read head.
// Must be aligned to a byte boundary.
func (r *Reader) Peek(n int) ([]byte, error) {
	if r.remaining != 0 {
		return nil, fmt.Errorf("unable to peek at next bytes: %w", ErrMisaligned)
	}
	return r.r.Peek(n)
}

// Discard skips the next n bytes in the reader.
// Must be aligned to a byte boundary.
func (r *Reader) Discard(n int) (int, error) {
	if r.remaining != 0 {
		return 0, fmt.Errorf("unable to discard next bytes: %w", ErrMisaligned)
	}
	return r.r.Discard(n)
}

// ReadByte returns the next byte in the reader.
// Must be aligned to a byte boundary.
func (r *Reader) ReadByte() (byte, error) {
	if r.remaining != 0 {
		return 0, fmt.Errorf("unable to read byte: %w", ErrMisaligned)
	}
	return r.r.ReadByte()
}
