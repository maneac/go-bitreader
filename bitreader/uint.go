package bitreader

import "fmt"

// ReadBool will read the next bit as a boolean
func (r *Reader) ReadBool() (bool, error) {
	boolBytes, err := r.ReadBits(1)
	if err != nil {
		return false, fmt.Errorf("reading boolean bit: %w", err)
	}
	return boolBytes[0] > 0, nil
}

// ReadUint8 will read the next n bits as a uint8.
func (r *Reader) ReadUint8(n uint) (uint8, error) {
	uintBytes, err := r.ReadBits(n)
	if err != nil {
		return 0, fmt.Errorf("reading uint8 bits: %w", err)
	}

	return uint8(uintBytes[0]), nil
}

// ReadUint16BE will read the next n bits as a Big-Endian uint16.
func (r *Reader) ReadUint16BE(n uint) (uint16, error) {
	uintBytes, err := r.ReadBits(n)
	if err != nil {
		return 0, fmt.Errorf("reading uint16 bits: %w", err)
	}

	var out uint16
	for i := 0; i < len(uintBytes); i++ {
		out |= (uint16(uintBytes[i]) << (8 * (len(uintBytes) - i - 1)))
	}

	return out, nil
}

// ReadUint16LE will read the next n bits as a Little-Endian uint16.
func (r *Reader) ReadUint16LE(n uint) (uint16, error) {
	uintBytes, err := r.ReadBits(n)
	if err != nil {
		return 0, fmt.Errorf("reading uint16 bits: %w", err)
	}

	var out uint16
	for i := 0; i < len(uintBytes); i++ {
		out |= (uint16(uintBytes[i]) << (8 * i))
	}

	return out, nil
}

// ReadUint32BE will read the next n bits as a Big-Endian uint32.
func (r *Reader) ReadUint32BE(n uint) (uint32, error) {
	uintBytes, err := r.ReadBits(n)
	if err != nil {
		return 0, fmt.Errorf("reading uint32 bits: %w", err)
	}

	var out uint32
	for i := 0; i < len(uintBytes); i++ {
		out |= (uint32(uintBytes[i]) << (8 * (len(uintBytes) - i - 1)))
	}

	return out, nil
}

// ReadUint32LE will read the next n bits as a Little-Endian uint32.
func (r *Reader) ReadUint32LE(n uint) (uint32, error) {
	uintBytes, err := r.ReadBits(n)
	if err != nil {
		return 0, fmt.Errorf("reading uint32 bits: %w", err)
	}

	var out uint32
	for i := 0; i < len(uintBytes); i++ {
		out |= (uint32(uintBytes[i]) << (8 * i))
	}

	return out, nil
}

// ReadUint64BE will read the next n bits as a Big-Endian uint64.
func (r *Reader) ReadUint64BE(n uint) (uint64, error) {
	uintBytes, err := r.ReadBits(n)
	if err != nil {
		return 0, fmt.Errorf("reading uint64 bits: %w", err)
	}

	var out uint64
	for i := 0; i < len(uintBytes); i++ {
		out |= (uint64(uintBytes[i]) << (8 * (len(uintBytes) - i - 1)))
	}

	return out, nil
}

// ReadUint64LE will read the next n bits as a Little-Endian uint64.
func (r *Reader) ReadUint64LE(n uint) (uint64, error) {
	uintBytes, err := r.ReadBits(n)
	if err != nil {
		return 0, fmt.Errorf("reading uint64 bits: %w", err)
	}

	var out uint64
	for i := 0; i < len(uintBytes); i++ {
		out |= (uint64(uintBytes[i]) << (8 * i))
	}

	return out, nil
}
